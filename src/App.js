import "./App.css";
import VButton from "./components/UI/Button/VButton.jsx";
import VInput from "./components/UI/VInput/VInput.jsx";
import PostList from "./components/PostList/PostList.jsx";
import VSort from "./components/VSort/VSort";
import PostForm from "./components/PostForm/PostForm.jsx";
import ModalForm from "./components/ModalForm/ModalForm.jsx";
import { mockPosts } from "./components/PostList/mockPosts.js";
import { useState } from "react";
import usePosts from "./components/PostList/usePosts.js";

function App() {
  const [posts, setPosts] = useState(mockPosts);
  const [selectedText, setSelectedText] = useState("");
  const [modal, setModal] = useState(false);
  const searchSortPosts = usePosts(posts, selectedText);

  const createPost = (post) => {
    setModal(false);
    setPosts([...posts, post]);
  };

  const removePost = (name) => {
    if (!name) return;

    const idx = posts.findIndex((el) => el.id === name);
    if (idx !== -1) {
      const temp = [...posts];
      temp.splice(idx, 1);
      setPosts(temp);
    }
  };

  const changeSort = (posts) => {
    setPosts(posts);
  };

  const searchPost = (event) => {
    const text = event.target ? event.target.value : event;
    setSelectedText(text);
  };

  return (
    <div className="App">
      <VButton onClick={() => setModal(true)} style={{ marginTop: 30 }}>
        Создать пост
      </VButton>
      <ModalForm visible={modal} setVisible={setModal}>
        <PostForm create={createPost}></PostForm>
      </ModalForm>

      <VInput onChange={searchPost} placeholder="Search..."></VInput>

      <VSort posts={posts} handleSort={changeSort}></VSort>
      <hr></hr>
      <PostList posts={searchSortPosts} remove={removePost}></PostList>
    </div>
  );
}

export default App;
