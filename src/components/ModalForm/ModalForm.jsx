import React, {useState} from "react";
import cl from "./ModalForm.module.css";

const ModalForm = ({children, visible, setVisible}) => {

  const rootClasses = [cl.modalForm]
  if (visible) {
    rootClasses.push(cl.active);
  }

  return (
    <div className={rootClasses.join(" ")} onClick={()=> setVisible(false)}>
      <div className={cl.modalFormContent} onClick={(e) => e.stopPropagation()}>{children}</div>
    </div>
  )

}

export default ModalForm;