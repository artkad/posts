import React, {useState} from "react";
import VButton from "../../components/UI/Button/VButton.jsx";
import VInput from "../../components/UI/VInput/VInput.jsx";

const PostForm = ({create}) => {
  const [post, setPost] = useState({ name: "", description: "" });

  const updateNewPost = (event) => {
    const target = event.target;
    setPost({ ...post, [target.name]: target.value });
  };

  const updatePost = () => {
    if (!post.name || !post.description) return;

    create({...post, id: Date.now()})
    setPost({ name: "", description: "" });
  };

  return (
    <div className="add">
    <VInput name="name" value={post.name} onChange={updateNewPost}  placeholder="Name..."></VInput>

    <VInput
      name="description"
      value={post.description}
      onChange={updateNewPost}
      placeholder="Description"
    ></VInput>

    <VButton  onClick={updatePost} name="Добавить">Добавить</VButton>
  </div>
  )
}

export default PostForm;