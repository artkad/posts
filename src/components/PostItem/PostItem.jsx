import React from "react";
import VButton from "../UI/Button/VButton.jsx";
import "./PostItem.css";

const PostItem = ({post, remove}) => (
  <div className="post">
  <div className="post-title">
    {/* <span>{idx + 1}. </span> */}
    <div className="post-name">{post.name}</div>
  </div>
  <div className="post-description">{post.description}</div>
  <VButton  onClick={()=>remove(post.id)}>Delete</VButton>
</div>
)

export default PostItem;
