import React from "react";
import PostItem from "../PostItem/PostItem.jsx";

const PostList = (props) =>
{
  if (!props.posts.length) {
    return ( <h1 style={{ textAlign: "center" }}>NO POSTS</h1>)
  }
  return (
    props.posts.map((post, idx) => (
     <PostItem post={post} remove={props.remove} key={post.id}></PostItem>
    ))
  )
}
 ;

export default PostList;
