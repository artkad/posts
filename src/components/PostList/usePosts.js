import { useMemo } from "react";

const usePosts = (posts, selectedText) => {
  return useMemo(() => {
    return posts.filter((post) =>
      post.name.toLowerCase().includes(selectedText.toLowerCase())
    );
  }, [posts, selectedText]);
};

export default usePosts;
