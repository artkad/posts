import React from "react";
import "./VButton.css"

const VButton = ({children, ...props}) => {
return (
  <button className="button" {...props}>{children}</button>
)
}

export default VButton;