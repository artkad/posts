import React from "react";
import classses from "./VInput.module.css";

const VInput = (props) => 
 (<div className={classses.VInput}> <input {...props} /> </div>)

export default VInput;