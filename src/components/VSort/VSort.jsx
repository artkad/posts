import React from "react";

const VSort = (props) => {

  const changeSort = (sort) => {
    const localPosts = [...props.posts];
    if (sort === "asc") {
      localPosts.sort(function (a, b) {
        if (a.name > b.name) {
          return 1;
        }
        if (a.name < b.name) {
          return -1;
        }
        // a должно быть равным b
        return 0;
      });
    }
    if (sort === "desc") {
      localPosts.sort(function (a, b) {
        if (a.name > b.name) {
          return -1;
        }
        if (a.name < b.name) {
          return 1;
        }
        // a должно быть равным b
        return 0;
      });
    }
    return localPosts
  };

  return (
    <select onChange={(e)  => props.handleSort(changeSort(e.target.value))}>
      <option value="asc">ASC</option>
      <option value="desc">DESC</option>
    </select>
  );
};

export default VSort;
